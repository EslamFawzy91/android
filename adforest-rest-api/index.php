<?php 
/**
* Plugin Name: AdForest Apps API
* Plugin URI: https://codecanyon.net/user/scriptsbundle
* Description: This plugin is essential for the AdFrest android and ios apps.
* Version: 2.0.1
* Author: Scripts Bundle
* Author URI: https://codecanyon.net/user/scriptsbundle
* License: GPL2
* Text Domain: adforest-rest-api
*/
 	/* Get Theme Info If There Is */
	$my_theme = wp_get_theme();
	$my_theme->get( 'Name' );
	/*Load text domain*/
	add_action( 'plugins_loaded', 'adforest_rest_api_load_plugin_textdomain' );
	function adforest_rest_api_load_plugin_textdomain()
	{
		load_plugin_textdomain( 'adforest-rest-api', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
	}	
	/*For Demo Only, If It's production make if true */
	define( 'ADFOREST_API_ALLOW_EDITING', true );		
 	/* Define Paths For The Plugin */
	define('ADFOREST_API_PLUGIN_FRAMEWORK_PATH', plugin_dir_path(__FILE__));	
	define('ADFOREST_API_PLUGIN_PATH', plugin_dir_path(__FILE__));	
	define('ADFOREST_API_PLUGIN_URL', plugin_dir_url(__FILE__));
	define('ADFOREST_API_PLUGIN_PATH_LANGS', plugin_dir_path(__FILE__). 'languages/' );	
	/*Theme Directry/Folder Paths */
	define( 'ADFOREST_API_THEMEURL_PLUGIN', get_template_directory_uri () . '/' );
	define( 'ADFOREST_API_IMAGES_PLUGIN', ADFOREST_API_THEMEURL_PLUGIN . 'images/');
	define( 'ADFOREST_API_CSS_PLUGIN', ADFOREST_API_THEMEURL_PLUGIN . 'css/');
	define( 'ADFOREST_API_JS_PLUGIN', ADFOREST_API_THEMEURL_PLUGIN . 'js/');

	/*Only check if plugin activate by theme */
	
 	$my_theme = wp_get_theme();
	if( $my_theme->get( 'Name' ) != 'adforest' && $my_theme->get( 'Name' ) != 'adforest child' )
	{
		require ADFOREST_API_PLUGIN_FRAMEWORK_PATH.'/tgm/tgm-init.php';	
	}
	/* Options Init */
	add_action('init', function(){
	  // Load the theme/plugin options
	  if (file_exists(dirname(__FILE__).'/inc/options-init.php')) {
		  require_once( dirname(__FILE__).'/inc/options-init.php' );
	  }
	});
	/*Added In Version 1.6.0 */
	if (!function_exists('adforestAPI_getallheaders'))
	{
		function adforestAPI_getallheaders()
		{
		   $headers = array();
		   foreach ($_SERVER as $name => $value)
		   {
			   if (substr($name, 0, 5) == 'HTTP_')
			   {
				   $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			   }
		   }
		   return $headers;
		}
	} 	
	$lang_code = $request_from = '';
	
	if (!function_exists('adforestAPI_getSpecific_headerVal'))
	{
		function adforestAPI_getSpecific_headerVal($header_key = '')
		{
			$header_val = '';
			if(count(adforestAPI_getallheaders()) > 0 )
			{
				foreach (adforestAPI_getallheaders() as $name => $value)
				{
					if( ($name == $header_key || $name == strtolower($header_key)) && $value != "" )
					{
						$header_val =  $value;
						break;
					}
				}
			}
			return $header_val;
		}
	}
	
	if (!function_exists('adforestAPI_set_lang_locale'))
	{
		function adforestAPI_set_lang_locale( $locale )
		{	
			$lang = adforestAPI_getSpecific_headerVal('Adforest-Lang-Locale');
			$lang = ( $lang  != "" ) ? $lang  : 'en';
			return ( $lang === 'en') ? 'en_US' : $lang;
		}
	}	
	/*add_filter('locale','adforestAPI_set_lang_locale',10);*/
	$request_from = adforestAPI_getSpecific_headerVal('Adforest-Request-From');
	define( 'ADFOREST_API_REQUEST_FROM', $request_from );
	/*Added In Version 1.6.0 */
	/*require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/inc/cpt.php';*/
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/inc/classes.php';
	/* Include Function  */
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/functions.php';	
	/* Include Classes */
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/inc/basic-auth.php';	
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/inc/auth.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/inc/email-templates.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/inc/categories-images.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/inc/notifications.php';	
	/* Include Other Classes */
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/settings.php';	
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/posts.php';	
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/home.php';	
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/users.php';	
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/index.php';	
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/ad_message.php';	
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/register.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/login.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/logout.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/ads.php';	
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/ad_post.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/bid.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/profile.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/woo-commerce.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/payment.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/push-notification.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/phone-verification.php';	
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/ad_rating.php';	
	/*Woo-Commerce Starts*/
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/woocommerce.php';
	require ADFOREST_API_PLUGIN_FRAMEWORK_PATH . '/classes/shop.php';
	
	/*Woo-Commerce ENds*/
/* Plugin Ends Here */